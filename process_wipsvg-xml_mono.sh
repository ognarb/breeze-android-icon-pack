#! /bin/bash

# get from todo/wip
for SVG in todo/wip/*.svg
do
N=$(basename ${SVG} .svg)
	inkscape -f ${SVG} -w 48 -h 48 -e \
		app/src/main/res/drawable-mdpi/${N}.png
	inkscape -f ${SVG} -w 72 -h 72 -e \
		app/src/main/res/drawable-hdpi/${N}.png
	inkscape -f ${SVG} -w 96 -h 96 -e \
		app/src/main/res/drawable-xhdpi/${N}.png
	inkscape -f ${SVG} -w 144 -h 144 -e \
		app/src/main/res/drawable-xxhdpi/${N}.png
	inkscape -f ${SVG} -w 192 -h 192 -e \
		app/src/main/res/drawable-xxxhdpi/${N}.png
cp ${SVG} ${SVG}.tmp
  scour --remove-descriptive-elements --enable-id-stripping --enable-viewboxing --enable-comment-stripping --nindent=4 -i ${SVG}.tmp -o ${SVG}
  rm ${SVG}.tmp
	mv ${SVG} icons
done
